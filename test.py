import json
import os

print('#!/bin/bash')

with open('input.json', 'r') as opf:
    json_obj = json.load(opf)
    obj = [json_obj]
for i in obj:
        print('export customer_name='+i['customer_name'])
        print('export request_id='+i['request_id'])
        print('export action='+i['action'])

