# Use an official OpenJDK runtime as a parent images
FROM openjdk:8-jre-alpine

# Set the working directory to /app
WORKDIR /home/ec2-user

# Copy the fat jar into the container at /home/ec2-user
COPY gs-spring-boot-1.0.1.jar /home/ec2-user

# Make port 8080 available to the world outside this container
EXPOSE  8083-8085  

# Run jar file when the container launches
CMD ["java", "-jar", "gs-spring-boot-1.0.1.jar"]
